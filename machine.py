water = 400
milk = 540
beans = 120
cups = 9
money = 550


# def print_state():
# 	global water, milk, beans, cups, money
# 	is_on = True
# 	while is_on:
# 		main_screen = input("Write action (buy, fill, take, remaining, exit): ")
# 		if main_screen == 'exit':
# 			is_on = False
# 		elif main_screen == "remaining":
# 			print(f"""The coffee machine has:
# 			{water} of water
# 			{milk} of milk
# 			{beans} of coffee beans
# 			{cups} of disposable cups
# 			{money} of money
# 			""")
# 		elif main_screen == "buy":
# 			buy()
# 		elif main_screen == "fill":
# 			fill()
# 		elif main_screen == "take":
# 			take_money()

def print_supplies():
	global water, milk, beans, cups, money
	print(f"""The coffee machine has:
{water} of water
{milk} of milk
{beans} of coffee beans
{cups} of disposable cups
{money} of money
""")


def main():
	while True:
		choice = input("Write action (buy, fill, take, remaining, exit): ")
		if choice == "buy":
			buy()
		elif choice == "fill":
			fill()
		elif choice == "take":
			take_money()
		elif choice == "remaining":
			print_supplies()
		elif choice == "exit":
			break


def buy():
	print("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino, back - to main menu:")
	coffee_type = input()
	
	if coffee_type == "back":
		return False
	elif coffee_type == '1':
		espresso()
	elif coffee_type == '2':
		latte()
	elif coffee_type == '3':
		cappuccino()


def espresso():
	global water, beans, cups, money
	if water <= 250:
		print(f"Sorry, not enough {water}!")
	elif beans <= 16:
		print(f"Sorry, not enough {beans}!")
	elif cups <= 0:
		print(f"Sorry, not enough {cups}!")
	else:
		water -= 250
		beans -= 16
		cups -= 1
		money += 4
		print("I have enough resources, making you a coffee!")


def latte():
	global water, milk, beans, cups, money
	if water <= 350:
		print(f"Sorry, not enough {water}!")
	elif milk <= 75:
		print(f"Sorry, not enough {milk}!")
	elif beans <= 20:
		print(f"Sorry, not enough {beans}!")
	elif cups <= 0:
		print(f"Sorry, not enough {cups}!")
	else:
		water -= 350
		milk -= 75
		beans -= 20
		cups -= 1
		money += 7
		print("I have enough resources, making you a coffee!")



def cappuccino():
	global water, milk, beans, cups, money
	if water <= 200:
		print(f"Sorry, not enough {water}!")
	elif milk <= 100:
		print(f"Sorry, not enough {milk}!")
	elif beans <= 12:
		print(f"Sorry, not enough {beans}!")
	elif cups <= 0:
		print(f"Sorry, not enough {cups}!")
	else:
		water -= 200
		milk -= 100
		beans -= 12
		cups -= 1
		money += 6
		print("I have enough resources, making you a coffee!")


def fill():
	global water, milk, beans, cups, money
	print("Write how many ml of water you want to add:")
	fill_water = int(input())
	water += fill_water
	print("Write how many ml of milk you want to add:")
	fill_milk = int(input())
	milk += fill_milk
	print("Write how many grams of coffee you want to add:")
	fill_beans = int(input())
	beans += fill_beans
	print("Write how many disposable cups you want to add:")
	fill_cups = int(input())
	cups += fill_cups


def take_money():
	global money
	print("I gave you $550")
	money = money - money


if __name__ == '__main__':
	main()

# def remaining():
# 	global water, milk, beans, cups, money
# 	print_state()


# def exit_program():
# 	exit()


# remaining()
# print_state()
# print("")
# print("Write action (buy, fill, take, remaining, exit):")
# user = input()
# if user == 'buy':
# 	buy()
# elif user == 'fill':
# 	fill()
# elif user == 'take':
# 	take_money()
# elif user == 'remaining':
# 	remaining()
# else:
# exit_program()

